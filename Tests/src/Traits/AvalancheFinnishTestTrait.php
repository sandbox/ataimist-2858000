<?php

namespace Drupal\Tests\avalanche\Traits;

use Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishStemmer;
use Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord;

/**
 * Provides testing methods for Avalanche Finnish stemmer.
 */
trait AvalancheFinnishTestTrait {

  use AvalanchePluginTestTrait;

  /**
   * Stemmer instance.
   *
   * @var \Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishStemmer
   */
  public $stemmer;

  /**
   * Sets up the stemmer to be tested.
   */
  public function setUp() {
    $this->stemmer = new FinnishStemmer();
  }

  /**
   * Tests FinnishStemmer with a data provider method.
   *
   * @param int $index
   *   Line in input (and output) file where the test case comes from.
   * @param string $word
   *   The word to be stemmed.
   * @param string $stem
   *   The expected stemming result.
   *
   * @dataProvider stemDataProvider
   */
  public function testStemmer($index, $word, $stem) {
    $word = FinnishWord::normalize($word);
    $stem = FinnishWord::normalize($stem);
    $result = $this->stemmer->stem($word)->getStem();

    $this->assertEquals(
        $stem,
        $result,
        "Line #$index failed, was $result, not $stem."
    );
  }

}
