<?php

namespace Drupal\Tests\avalanche\Traits;

/**
 * Provides an universal testing method for Avalanche stemmers.
 */
trait AvalanchePluginTestTrait {

  /**
   * Gets test data for the specified language.
   *
   * @param string $directory
   *   The directory path where the test file is located.
   * @param string $language
   *   The language to get the vocabulary for. Should match a subdirectory in
   *   the unit tests directory (e.g. 'Finnish') that contains input.txt (the
   *   words to be stemmed) and output.txt (the expected results).
   * @param int $count
   *   (optional) Number of words to test. Default: 1000.
   * @param int $offset
   *   (optional) Number of words (lines) to skip from input.txt and output.txt
   *   before getting the $count words. Default: 0.
   *
   * @return array
   *   A multidimensional array of input words and their expected stems.
   */
  public function getData($directory, $language, $count = 1000, $offset = 0) {
    $data = [];
    $directory = $directory . '/' . $language;
    $input_file = file($directory . '/input.txt');
    $input_size = count($input_file);
    $output_file = file($directory . '/output.txt');
    $output_size = count($output_file);

    // Check that the input file isn't empty.
    $this->assertTrue(
        $input_size > 0,
        $language . ' vocabulary input file is empty.'
    );

    // Check that input and output files have the same number of lines, as
    // we're expecting line #10 in output.txt to be the stemmed version of line
    // #10 in input.txt.
    $this->assertEquals(
        $input_size,
        $output_size,
        $language . ' vocabulary input and output files differ in line count.'
    );

    // Can't give more lines than there are in the file.
    if ($count > $input_size) {
      $count = $input_size;
    }
    // If there aren't enough lines in the file to give $count lines after
    // $offset, adjust $offset so that we get enough lines.
    if ($offset + $count > $input_size) {
      $offset = $input_size - $count;
    }

    $input = array_slice($input_file, $offset, $count);
    $output = array_slice($output_file, $offset, $count);

    foreach ($input as $index => $word) {
      $data[] = [
        // Add +1 to line number (arrays start from 0, but line numbers don't).
        $index + 1,
        $word,
        $output[$index],
      ];
    }

    return $data;
  }

}
