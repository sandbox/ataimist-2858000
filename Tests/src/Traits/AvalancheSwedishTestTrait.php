<?php

namespace Drupal\Tests\avalanche\Traits;

use Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishStemmer;
use Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishWord;

/**
 * Provides testing methods for Avalanche Swedish stemmer.
 */
trait AvalancheSwedishTestTrait {

  use AvalanchePluginTestTrait;

  /**
   * Stemmer instance.
   *
   * @var \Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishStemmer
   */
  public $stemmer;

  /**
   * Sets up the stemmer to be tested.
   */
  public function setUp() {
    $this->stemmer = new SwedishStemmer();
  }

  /**
   * Tests SwedishStemmer with a data provider method.
   *
   * @param int $index
   *   Line in input (and output) file where the test case comes from.
   * @param string $word
   *   The word to be stemmed.
   * @param string $stem
   *   The expected stemming result.
   *
   * @dataProvider stemDataProvider
   */
  public function testStemmer($index, $word, $stem) {
    $word = SwedishWord::normalize($word);
    $stem = SwedishWord::normalize($stem);
    $result = $this->stemmer->stem($word)->getStem();

    $this->assertEquals(
        $stem,
        $result,
        "Line #$index failed, was $result, not $stem."
    );
  }

}
