<?php

namespace Drupal\Tests\avalanche\Unit;

use Drupal\Tests\UnitTestCase;

/**
 * Tests the "SwedishStemmer" Avalanche stemmer plugin implementation.
 *
 * Very basic test, modeled after unit tests in the porterstemmer module. Test
 * data is from http://snowballstem.org/algorithms/swedish/stemmer.html.
 *
 * Covers test strings 25001-30000.
 *
 * @coversDefaultClass \Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishStemmer
 * @group avalanche
 *
 * @see \Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishStemmer
 */
class AvalancheSwedishTest6 extends UnitTestCase {

  use \Drupal\Tests\avalanche\Traits\AvalancheSwedishTestTrait;

  /**
   * Provides data for testStemmer().
   *
   * @return array
   *   Nested arrays of values to check:
   *   - $word
   *   - $stem
   */
  public function stemDataProvider() {
    return $this->getData(__DIR__, 'Swedish', 5000, 25000);
  }

}
