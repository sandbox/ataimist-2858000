<?php

namespace Drupal\Tests\avalanche\Unit;

use Drupal\Tests\UnitTestCase;

/**
 * Tests the "FinnishStemmer" Avalanche stemmer plugin implementation.
 *
 * Very basic test, modeled after unit tests in the porterstemmer module. Test
 * data is from http://snowballstem.org/algorithms/finnish/stemmer.html.
 *
 * Covers test strings 15001-20000.
 *
 * @coversDefaultClass \Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishStemmer
 * @group avalanche
 *
 * @see \Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishStemmer
 */
class AvalancheFinnishTest4 extends UnitTestCase {

  use \Drupal\Tests\avalanche\Traits\AvalancheFinnishTestTrait;

  /**
   * Provides data for testStemmer().
   *
   * @return array
   *   Nested arrays of values to check:
   *   - $word
   *   - $stem
   */
  public function stemDataProvider() {
    return $this->getData(__DIR__, 'Finnish', 5000, 15000);
  }

}
