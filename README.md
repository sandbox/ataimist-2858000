Avalanche
---------------

### About this Module

Avalanche is a collection of search stemmers that help improve database search
by reducing words in searches to their stems. The same stemming process takes
place during search indexing.

Consider, for example, some of the different forms of the word 'institution'
in the Finnish language: instituutio, instituutioiden, instituutioihin,
instituutioilla, instituutioille, instituutioina, instituutioissa,
instituutioista, instituutioita, instituutioitaan, instituutioksi,
instituutiolla, instituution, instituutiona, instituutioon, instituutiossa,
instituutiosta, instituutiot, instituutiota (no, that's not all; sorry).
With a perfect stemmer, all of these get stemmed to a single common form, in
this case, 'instituutio' - both during indexing and when users search for them.
Stemmers are rarely perfect, of course, but a decent stemmer is better than no
stemmer at all.

If you're using a search backend other than the default database search, e.g.
Apache Solr, you might want to check if it can do the stemming for you.
There is currently no support for using Avalanche with other search backends;
it might work or it might not. Furthermore, the same algorithms used by this
module are available in (at least) Solr.

### Installing Avalanche

Download and enable the module. Avalanche is self-contained; no external files
are required. When the module is enabled, it will automatically start stemming
any languages with suitable stemmer plugins, using default settings specified
for each plugin. You should review these settings to make sure that they suit
your needs - you probably don't want to stem your product name so that it
matches something funny in the search.

### Configuring Avalanche

General settings for Avalanche itself can be found in admin/config/avalanche;
configuration pages for each enabled language supported by Avalanche appear as
tabs on that page. Stemmer settings are language-specific and can be
found on the language pages. Configuration options for Avalanche itself are
as follows:

- Override core transliteration (default: enabled / on)
  - Replaces the core transliteration service with a near-exact duplicate that
    doesn't remove diacritical marks (which interferes with some stemmers).
    Without this, tähti 'star' is equal to tahti 'beat', along with numerous
    other examples in various languages. Safe to disable, but recommended if
    dealing with any languages in which diacritical marks actually alter the
    meaning of words. @see issue 2858337 (https://www.drupal.org/node/2858337).
- Disable all stemmers (default: disabled / off)
  - Stops Avalanche from stemming anything, regardless of individual stemmer
    configuration. Since this doesn't disable the module itself, site-wide
    settings such as the core transliteration override can still be in effect.
    This option should generally not be enabled, except for debugging and
    testing purposes.
- Use global stopwords (default: disabled / off)
  - If enabled, uses (and allows you to input) a comma-separated list of words
    that will never be touched by ANY stemmer, regardless of their individual
    stopword configuration. Mostly useful for words that do not change between
    languages, such as product or company names.

Individual stemmers have their own, separate configuration pages which appear
as tabs on the configuration page of every enabled language the stemmer
supports. Settings are language-specific: if a stemmer supports more than one
language, it can, for example, be disabled for one and enabled for the other.
Available settings vary by the capabilities of each stemmer, but at minimum the
following will be found:

- Disable stemmer (default: disabled / off)
  - Disables the stemmer for the current language. If there are no other
    stemmers capable of stemming the current language or none are enabled,
    input in this language will no longer be stemmed.
- Minimum word length (default: varies by language)
  - Allows you to specify a minimum length for stemmable words. Words shorter
    than the specified number of characters will not be stemmed.
- Stemmer-specific stopwords (default: empty)
  - Allows you to input a comma-separated list of words that will not be
    touched by this stemmer. Used to prevent stemming of specific words.
