<?php

namespace Drupal\avalanche;

/**
 * Stopword interface.
 */
interface StopwordInterface {

  /**
   * Checks if a given word is a stopword.
   *
   * @param string $word
   *   The word to check.
   */
  public static function matchStopword($word);

}
