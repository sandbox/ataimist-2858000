<?php

namespace Drupal\avalanche\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Stemmer type annotation object.
 *
 * Stemmer classes define stemmer plugins for the Avalanche module, which uses
 * them with the core Search module to stem search input (both during indexing
 * and actual searches).
 *
 * @ingroup avalanche
 *
 * @Annotation
 */
class Stemmer extends Plugin {

  /**
   * A unique identifier for the stemmer.
   *
   * @var string
   */
  public $id;

  /**
   * The language codes that the stemmer can handle.
   *
   * @var array
   */
  public $languages;

  /**
   * The title for the stemmer configuration tab.
   *
   * @var string
   */
  public $title;

}
