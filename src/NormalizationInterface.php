<?php

namespace Drupal\avalanche;

/**
 * Normalization interface.
 */
interface NormalizationInterface {

  /**
   * Normalizes words for the stemmer according to the language's rules.
   *
   * Word classes can implement a function for normalizing characters that
   * appear in the language; see the Porter Stemmer introduction at
   * http://snowballstem.org/texts/introduction.html.
   */
  public static function normalize($word);

}
