<?php

namespace Drupal\avalanche;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Avalanche plugin manager.
 *
 * Finds stemmer plugins and makes them work. Also tells the controller all
 * their secrets.
 *
 * @see \Drupal\avalanche\Annotation\Stemmer
 * @see \Drupal\avalanche\StemmerInterface
 * @see \Drupal\avalanche\Controller\AvalancheController
 * @see plugin_api
 */
class AvalanchePluginManager extends DefaultPluginManager {
  /**
   * Array of available stemmer plugins, keyed by language code.
   *
   * @var array
   */
  protected $plugins = [];
  /**
   * Array of interfaces implemented by each stemmer class, keyed by plugin ID.
   *
   * @var array
   */
  protected $interfaces = [];
  /**
   * Array of the language codes supported by the available stemmer plugins.
   *
   * @var array
   */
  protected $languages = [];
  /**
   * Array of \Drupal\avalanche\StemmerInterface stemmers, keyed by language.
   *
   * @var array
   */
  protected $stemmers = [];

  /**
   * Constructs an AvalanchePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Avalanche',
      $namespaces,
      $module_handler,
      'Drupal\avalanche\StemmerInterface',
      'Drupal\avalanche\Annotation\Stemmer'
    );
    $this->alterInfo('avalanche_stemmers');
    $this->setCacheBackend($cache_backend, 'avalanche_stemmers');

    $this->plugins = $this->getPlugins();
    $this->languages = $this->getSupportedLanguages();
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    // Only create a single instance of each stemmer plugin.
    if (!empty($this->stemmers[$plugin_id])) {
      return $this->stemmers[$plugin_id];
    }

    $plugin_definition = $this->getDefinition($plugin_id);
    $plugin_class = DefaultFactory::getPluginClass(
      $plugin_id,
      $plugin_definition,
      'Drupal\avalanche\StemmerInterface'
    );

    $this->stemmers[$plugin_id] = new $plugin_class(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    return $this->stemmers[$plugin_id];
  }

  /**
   * Gets the interfaces implemented by a plugin class.
   *
   * Internal function, does processing. Use getStemmerInterfaces() to get this
   * information from outside this class.
   *
   * @param string $plugin_id
   *   ID of the plugin to get interfaces for.
   *
   * @return array
   *   Array of interfaces implemented by the given class.
   */
  private function getPluginInterfaces($plugin_id) {
    $plugin_definition = $this->getDefinition($plugin_id);
    $plugin_class = DefaultFactory::getPluginClass($plugin_id, $plugin_definition, 'Drupal\avalanche\StemmerInterface');
    $interfaces = class_implements($plugin_class);

    // Filter out interfaces not provided by Avalanche.
    array_walk($interfaces, function (&$n) {
      if (strpos($n, 'Drupal\avalanche') === FALSE) {
        $n = NULL;
      }
    });

    $interfaces = array_filter($interfaces);
    // Only return the interface name without its namespace.
    $interfaces = str_replace('Drupal\\avalanche\\', '', $interfaces);

    return $interfaces;
  }

  /**
   * Gets all available stemmer plugins.
   *
   * @return array
   *   Array of available stemmer plugins.
   */
  public function getPlugins() {
    if (empty($this->plugins)) {
      $this->plugins = $this->getDefinitions();
    }

    return $this->plugins;
  }

  /**
   * Gets all stemmers and their supported languages, sorted by language.
   *
   * @return array
   *   Array of available stemmer IDs sorted by their supported language codes.
   */
  public function getSupportedLanguages() {
    if (empty($this->languages)) {
      $plugins = $this->getPlugins();
      $plugin_languages = array_column($plugins, 'languages');
      $languages = call_user_func_array('array_merge', $plugin_languages);
      $this->languages = array_fill_keys(array_unique($languages), []);

      foreach ($plugins as $plugin) {
        foreach ($plugin['languages'] as $langcode) {
          $this->languages[$langcode][] = $plugin['id'];
        }
      }
    }

    return $this->languages;
  }

  /**
   * Gets the IDs of all stemmers for a specific language.
   *
   * @param string $language
   *   The language (e.g. 'fi' or 'sv') to find stemmers for.
   *
   * @return array
   *   IDs of stemmers for the given language (empty array if none exist).
   *
   * @todo Should cache results until plugin configuration changes.
   */
  public function getStemmersByLanguage($language) {
    $languages = $this->getSupportedLanguages();

    if (empty($languages[$language])) {
      return [];
    }

    return $languages[$language];
  }

  /**
   * Gets the interfaces implemented by a specific stemmer plugin.
   *
   * @param string $id
   *   The ID of the stemmer to find interfaces for.
   *
   * @return array
   *   Array of the interfaces implemented by the stemmer class.
   */
  public function getStemmerInterfaces($id) {
    if (empty($this->interfaces[$id])) {
      $this->interfaces[$id] = $this->getPluginInterfaces($id);
    }

    return $this->interfaces[$id];
  }

}
