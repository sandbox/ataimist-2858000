<?php

namespace Drupal\avalanche;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the transliteration service.
 */
class AvalancheServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides the core transliteration service to prevent it from changing
    // search input. See Drupal\avalanche\AvalancheTransliteration.
    $definition = $container->getDefinition('transliteration');
    $definition->setClass('Drupal\avalanche\AvalancheTransliteration');
  }

}
