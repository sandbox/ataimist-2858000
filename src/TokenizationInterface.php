<?php

namespace Drupal\avalanche;

/**
 * Tokenization interface.
 */
interface TokenizationInterface {

  /**
   * Extracts tokens from a given string.
   *
   * @param string $string
   *   String to tokenize.
   * @param int $min_length
   *   (optional) Minimum length of a token. Default: 3.
   * @param array $discard_tokens
   *   (optional) Array of tokens to discard.
   */
  public static function tokenize($string, $min_length = 3, array $discard_tokens = NULL);

}
