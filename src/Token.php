<?php

namespace Drupal\avalanche;

/**
 * Token base class.
 */
abstract class Token {

  use TokenTrait;

  /**
   * Creates a token instance.
   *
   * Each word class should accept the word to be stemmed as an argument.
   *
   * @param string $word
   *   The word to be stemmed.
   */
  abstract public function __construct($word);

  /**
   * Finds the regions (R1 and R2) in a word.
   *
   * Word classes should implement a function for determining word regions.
   * How the function does this varies from algorithm to algorithm.
   */
  abstract protected function getRegion();

}
