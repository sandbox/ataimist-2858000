<?php

namespace Drupal\avalanche\Form;

use Drupal\avalanche\AvalanchePluginManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a general configuration form for Avalanche.
 */
class AvalancheSettingsForm extends ConfigFormBase {

  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(AvalanchePluginManager $pluginManager) {
    $this->pluginManager = $pluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('plugin.manager.avalanche')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'avalanche_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'avalanche.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $module_configuration = $this->config('avalanche.settings');

    $options = [
      'context' => 'Avalanche',
    ];

    // Warn if all stemmers are globally disabled.
    if ($module_configuration->get('disable')) {
      drupal_set_message(
          $this->t(
            'Avalanche is currently <strong>disabled</strong>. To enable stemming according to individual stemmer configuration, uncheck the <em>@disable</em> option.',
            [
              '@disable' => $this->t(
                  'Disable all stemmers',
                  [],
                  $options
              ),
            ],
            $options
          ),
          'warning'
      );
    }

    // Warn if global stopwords are set but are not used.
    if (!$module_configuration->get('stopwords') && !empty($module_configuration->get('stopwords_list'))) {
      drupal_set_message(
          $this->t(
            'You have specified one or more global stopwords, but they will not be used as the <em>@stopwords</em> option is disabled.',
            [
              '@stopwords' => $this->t(
                  'Use global stopwords',
                  [],
                  $options
              ),
            ],
            $options
          ),
          'warning'
      );
    }

    // Introduction text at the top of the form, including links to stemmers.
    $form['intro'] = [
      '#markup' => $this->t(
        '<p>This page allows you to configure global settings for Avalanche. These settings affect all Avalanche stemmers.</p><p>Settings for available stemmers for each enabled language are located on the language tabs.</p>',
        [],
        $options
      ),
    ];

    // Checkbox for enabling or disabling core transliteration service override.
    $form['override_transliteration'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t(
        'Override core transliteration',
        array(),
        $options
      ),
      '#description' => $this->t(
        'If enabled, Avalanche will prevent the core transliteration service from removing diacritical marks from search input. For more information, see <a href=":url">issue #2858337</a>.',
        [':url' => 'https://www.drupal.org/node/2858337'],
        $options
      ),
      '#default_value' => $module_configuration->get('override_transliteration'),
    );

    // Checkbox for globally disabling all stemmers.
    $form['disable'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t(
        'Disable all stemmers',
        array(),
        $options
      ),
      '#description' => $this->t(
        'If enabled, Avalanche will not alter search input during search or indexing, effectively disabling the module (except for overriding core transliteration, if enabled).',
        array(),
        $options
      ),
      '#default_value' => $module_configuration->get('disable'),
    );

    // Checkbox for enabling or disabling global stopwords (see below).
    $form['stopwords'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t(
        'Use global stopwords',
        array(),
        $options
      ),
      '#description' => $this->t(
        'If enabled, allows you to provide a list of words that will be excluded from all stemming.',
        array(),
        $options
      ),
      '#default_value' => !empty($module_configuration->get('stopwords')),
    );

    // Textarea for setting global (stemmer independent) stopwords.
    $stopwords = $module_configuration->get('stopwords_list');
    $form['stopwords_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t(
        'Global stopwords',
        array(),
        $options
      ),
      '#description' => $this->t(
        'Comma-separated list of words (foo, bar, baz), such as product names, which will be excluded from stemming by all stemmers, regardless of their individual stopword settings.',
        array(),
        $options
      ),
      '#states' => array(
        // Show when the 'Use global stopwords' checkbox is enabled.
        'visible' => array(
          ':input[name="stopwords"]' => array('checked' => TRUE),
        ),
      ),
      '#default_value' => empty($stopwords) ? '' : implode(', ', $stopwords),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Turn the comma-separated stopwords list into an array of unique words.
    $stopwords = array_unique(
      array_map('trim', explode(',', $form_state->getValue('stopwords_list')))
    );

    $this->config('avalanche.settings')
      ->set('override_transliteration', $form_state->getValue('override_transliteration'))
      ->set('disable', $form_state->getValue('disable'))
      ->set('stopwords', $form_state->getValue('stopwords'))
      // Only save non-empty stopwords (i.e. prevent multiple comma confusion).
      ->set('stopwords_list', array_values(array_filter($stopwords)))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
