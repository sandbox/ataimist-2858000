<?php

namespace Drupal\avalanche\Form;

use Drupal\avalanche\AvalanchePluginManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for individual Avalanche stemmers.
 */
class AvalancheStemmerSettingsForm extends ConfigFormBase {

  protected $pluginManager;
  protected $languageManager;
  protected $routeMatch;
  protected $pluginId;
  protected $stemmer;
  protected $configName;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, AvalanchePluginManager $pluginManager, LanguageManagerInterface $languageManager, RouteMatchInterface $route_match) {
    parent::__construct($config_factory);
    $this->pluginManager = $pluginManager;
    $this->languageManager = $languageManager;
    $this->routeMatch = $route_match;
    $this->pluginId = $this->routeMatch->getParameter('stemmer');
    $this->language = $this->routeMatch->getParameter('language');
    $this->configName = 'avalanche.settings.' . $this->language . '.' . $this->pluginId;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.avalanche'),
      $container->get('language_manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'avalanche_stemmer_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this->configName];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->stemmer = $this->pluginManager->createInstance($this->pluginId);
    $config = $this->config($this->configName);
    $defaults = $this->stemmer->getConfiguration();
    $definition = $this->stemmer->getPluginDefinition();
    $interfaces = $this->pluginManager->getStemmerInterfaces($definition['id']);
    $language = $this->languageManager->getLanguageName($this->language);

    // Options for translation function.
    $options = [
      'context' => 'Avalanche',
    ];

    // Introduction text at the top of the form.
    $form['intro'] = [
      '#markup' => $this->t(
        'This page allows you to configure <strong>@name</strong> for the <strong>@language</strong> language. It implements the following interfaces: <strong>@interfaces</strong>',
        [
          '@name' => $definition['id'],
          '@language' => $language,
          '@interfaces' => implode(', ', $interfaces),
        ],
        $options
      ),
    ];

    // Checkbox for disabling the stemmer.
    $disable = $config->get('disable');
    $form['disable'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t(
        'Disable stemmer',
        array(),
        $options
      ),
      '#description' => $this->t(
        'If enabled, Avalanche will not use @stemmer to stem @language.',
        [
          '@stemmer' => $definition['id'],
          '@language' => $language,
        ],
        $options
      ),
      '#default_value' => isset($disable) ? $disable : $defaults['disable'],
    );

    // Number field for setting the minimum length of stemmable words.
    $min_length = $config->get('min_length');
    $form['min_length'] = [
      '#type' => 'number',
      '#title' => $this->t(
        'Minimum word length',
        array(),
        $options
      ),
      '#description' => $this->t(
        'Minimum length for stemmable words. Words shorter than the specified number of characters will not be stemmed. Enter 0 to stem words of any length.',
        array(),
        $options
      ),
      '#min' => 0,
      '#step' => 1,
      '#size' => 2,
      '#default_value' => empty($min_length) ? 0 : $min_length,
    ];

    // Textarea for setting stopwords.
    $stopwords = $config->get('stopwords_list');
    $form['stopwords_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t(
        'Stemmer-specific stopwords',
        array(),
        $options
      ),
      '#description' => $this->t(
        'Comma-separated list of words (foo, bar, baz), such as product names, which will be excluded from stemming.',
        array(),
        $options
      ),
      '#default_value' => empty($stopwords) ? '' : implode(', ', $stopwords),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Turn the comma-separated stopwords list into an array of unique words.
    $stopwords = array_unique(
      array_map('trim', explode(',', $form_state->getValue('stopwords_list')))
    );

    $this->config($this->configName)
      ->set('disable', $form_state->getValue('disable'))
      ->set('min_length', $form_state->getValue('min_length'))
      // Only save non-empty stopwords (i.e. prevent multiple comma confusion).
      ->set('stopwords_list', array_values(array_filter($stopwords)))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
