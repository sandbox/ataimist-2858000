<?php

namespace Drupal\avalanche\Form;

use Drupal\avalanche\AvalanchePluginManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for languages supported by available stemmers.
 */
class AvalancheLanguageSettingsForm extends ConfigFormBase {

  protected $pluginManager;
  protected $languageManager;
  protected $routeMatch;
  protected $language;
  protected $stemmer;
  protected $configName;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, AvalanchePluginManager $pluginManager, LanguageManagerInterface $languageManager, RouteMatchInterface $route_match) {
    parent::__construct($config_factory);
    $this->pluginManager = $pluginManager;
    $this->languageManager = $languageManager;
    $this->routeMatch = $route_match;
    $this->language = $this->routeMatch->getParameter('language');
    $this->configName = 'avalanche.settings.' . $this->language;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.avalanche'),
      $container->get('language_manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'avalanche_language_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [$this->configName];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $language_name = $this->languageManager->getLanguageName($this->language);

    // Options for translation function.
    $options = [
      'context' => 'Avalanche',
    ];

    // Introduction text at the top of the form.
    $form['intro'] = [
      '#markup' => $this->t(
        'This page allows you to configure <strong>@language</strong> stemmers.',
        [
          '@language' => $language_name,
        ],
        $options
      ),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
