<?php

namespace Drupal\avalanche;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Component\Transliteration\PhpTransliteration;

/**
 * Replacement for core transliteration.
 *
 * The 8.x change "Search removes diacritics in indexing rather than relying on
 * database collation" described in https://www.drupal.org/node/2447357 (based
 * on the issue at https://www.drupal.org/node/731298) is incompatible with
 * several languages. For example, Finnish tähti 'star' becomes tahti 'beat'.
 *
 * With the exception of removeDiacritics(), exactly mimics the core service.
 *
 * @see https://www.drupal.org/node/2858337
 */
class AvalancheTransliteration extends PhpTransliteration implements TransliterationInterface {

  /**
   * {@inheritdoc}
   */
  public function removeDiacritics($string) {
    // If transliteration override is disabled in Avalanche settings, call the
    // original removeDiacritics function from PhpTransliteration.
    $override = \Drupal::config('avalanche.settings')->get('override_transliteration');

    if (!$override) {
      $string = parent::removeDiacritics($string);
    }

    return $string;
  }

}
