<?php

namespace Drupal\avalanche;

/**
 * Trait for methods and properties shared by all Token classes.
 */
trait TokenTrait {

  /**
   * The original word that is being stemmed.
   *
   * @var string
   */
  protected $word;
  /**
   * The current stem of the word being stemmed. Changes during stemming.
   *
   * @var string
   */
  protected $stem;
  /**
   * The byte offset in the original word at which region 1 starts.
   *
   * @var int
   */
  protected $r1;
  /**
   * The byte offset in the original word at which region 2 starts.
   *
   * @var int
   */
  protected $r2;
  /**
   * An array of named patterns that will be replaced with the specified value.
   *
   * @var array
   */
  protected $namedPatterns = [];

  /**
   * Returns the current word stem.
   *
   * @return string
   *   The current stem.
   */
  public function getStem() {
    return $this->stem;
  }

  /**
   * Sets the current word stem (rarely used).
   *
   * @param string $stem
   *   The string that the stem should be set to.
   *
   * @return string
   *   The stem that was set.
   */
  public function setStem($stem) {
    $this->stem = $stem;

    return $this->stem;
  }

  /**
   * Returns all defined regions for the word.
   *
   * @return array
   *   Array with all defined regions for the word. Can be empty.
   */
  public function getRegions() {
    $regions = [
      'R1' => $this->r1,
      'R2' => $this->r2,
    ];

    return array_filter($regions);
  }

  /**
   * Adds a string to the end of the current stem.
   *
   * @param string $suffix
   *   The string to add.
   *
   * @return string
   *   The stem after the suffix has been inserted.
   */
  public function insertSuffix($suffix) {
    $this->stem .= (string) $suffix;

    return $this->stem;
  }

  /**
   * Deletes one or more characters from the stem, starting at the given offset.
   *
   * Deletes a given number of characters (N.B. NOT BYTES) from the current
   * word stem (depends on current step), starting from the given offset.
   * With a negative $start (e.g. -3) and no $length, deletes abs($start)
   * characters (e.g. 3) from the end of the string (think substr).
   *
   * @param int $start
   *   Character offset at which to start.
   * @param int $length
   *   (optional)  Number of characters to delete.
   *
   * @return string
   *   The current word stem after the deletion.
   */
  public function deleteSuffix($start, $length = NULL) {
    // Split the current stem into an array of individual characters.
    $stem = preg_split('//u', $this->stem, NULL, PREG_SPLIT_NO_EMPTY);

    // Negative $start with no $length = start from abs($start)'th character
    // from the end of the stem and delete anything that comes after.
    if ($start < 0 && !$length) {
      $start = count($stem) + $start;
      $length = count($stem) - $start;
    }

    // Delete $length characters from the stem, starting at $start.
    for ($i = 0; $i < $length; $i++) {
      unset($stem[$start + $i]);
    }

    // Now that the requested characters have been removed, join the array of
    // individual characters into a single string again.
    $this->stem = implode($stem);

    return $this->stem;
  }

  /**
   * Checks the word stem for the given PCRE pattern at the given offset.
   *
   * Checks if the current (depends on current step) stem of the word contains
   * the given pattern within the substring defined by the given offset. If it
   * does, also stores the offset of the match in $this->matches. Returns an
   * array of matches (see preg_match documentation for details), which may be
   * empty if there are none (empty arrays == FALSE and non-empty == TRUE, so
   * feel free to use the return value in an if statement or similar).
   *
   * @param string $pattern
   *   PCRE (preg_match) pattern to look for. Can contain named patterns that
   *   will be replaced with specified values before using the pattern in the
   *   preg_match call.
   * @param int|string $offset
   *   (optional) Byte offset after which the pattern must appear.
   *   You can use AVALANCHE_R1 and AVALANCHE_R2 to denote start of region 1 or
   *   2, respectively. Other strings are ignored (treated as 0). Default is 0,
   *   which means the start of the string.
   *
   * @return array
   *   A non-empty array of matches if the pattern was matched, or an empty
   *   array if the pattern didn't match.
   */
  public function hasPattern($pattern, $offset = 0) {
    // Array for holding matches (see preg_match documentation).
    $matches = [];

    // Accept AVALANCHE_R1 and AVALANCHE_R2 to denote start of region 1 or 2,
    // respectively; all other non-integer values are treated as 0. Note that
    // comparing with constants requires strict comparison.
    if ($offset === AVALANCHE_R1) {
      $offset = $this->r1;
    }
    elseif ($offset === AVALANCHE_R2) {
      $offset = $this->r2;
    }
    else {
      $offset = is_int($offset) ? $offset : 0;
    }

    // Replace custom named patterns (e.g. <%V%>) in $pattern with their
    // variable counterparts (e.g. $vowel).
    foreach ($this->namedPatterns as $named_pattern => $replacement) {
      $pattern = str_replace($named_pattern, $replacement, $pattern);
    }

    $query = preg_match($pattern, $this->stem, $matches, PREG_OFFSET_CAPTURE, $offset);

    // If a match is found ($query is TRUE), check that the match begins after
    // the given offset before confirming it. This is because all except one of
    // the pattern checks in this stemmer are restricted to one of the word's
    // regions. If there is no offset, we obviously don't need to check.
    if ($offset != 0 && $query) {
      // If the match begins before $offset, it's not actually a match.
      if ($matches[0][1] < $offset) {
        return [];
      }
    }

    return $matches;
  }

}
