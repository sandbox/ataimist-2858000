<?php

namespace Drupal\avalanche\Routing;

use Drupal\avalanche\AvalanchePluginManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines dynamic routes for Avalanche stemmer configuration pages.
 */
class AvalancheRoutes implements ContainerInjectionInterface {

  protected $pluginManager;
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(AvalanchePluginManager $pluginManager, LanguageManagerInterface $languageManager) {
    $this->pluginManager = $pluginManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('plugin.manager.avalanche'),
        $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = array();

    $languages = $this->languageManager->getLanguages();
    $stemmers = $this->pluginManager->getPlugins();
    $stemmable = $this->pluginManager->getSupportedLanguages();

    $items = array_intersect(
      array_keys($languages),
      array_keys($stemmable)
    );

    foreach ($items as $langcode) {
      $id = 'avalanche.settings.' . $langcode;
      $routes[$id] = new Route(
        // Path to attach this route to:
        '/admin/config/avalanche/' . $langcode,
        // Route defaults:
        [
          '_form' => '\Drupal\avalanche\Form\AvalancheLanguageSettingsForm',
          '_title' => $languages[$langcode]->getName(),
          'language' => $langcode,
        ],
        // Route requirements:
        [
          '_permission'  => 'administer site configuration',
        ],
        // Route options:
        [
          'language' => 'String',
        ]
      );
    }

    foreach ($stemmers as $stemmer) {
      if (!is_array($stemmer['languages'])) {
        $stemmer['languages'] = [$stemmer['languages']];
      }

      // Stemmers might support more than one language.
      foreach ($stemmer['languages'] as $language) {
        $id = 'avalanche.settings.' . $language . '.' . $stemmer['id'];
        $routes[$id] = new Route(
          // Path to attach this route to:
          '/admin/config/avalanche/' . $language . '/' . $stemmer['id'],
          // Route defaults:
          [
            '_form' => '\Drupal\avalanche\Form\AvalancheStemmerSettingsForm',
            '_title_callback' => '\Drupal\avalanche\Controller\AvalancheController::getStemmerSettingsFormTitle',
            'language' => $language,
            'stemmer' => $stemmer['id'],
          ],
          // Route requirements:
          [
            '_permission'  => 'administer site configuration',
          ],
          // Route options:
          [
            'language' => 'String',
            'stemmer' => 'String',
          ]
        );
      }
    }

    return $routes;
  }

}
