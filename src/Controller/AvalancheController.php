<?php

namespace Drupal\avalanche\Controller;

use Drupal\avalanche\AvalanchePluginManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Avalanche controller.
 */
class AvalancheController extends ControllerBase {
  /**
   * The plugin manager which handles the stemmer plugins.
   *
   * @var \Drupal\avalanche\AvalanchePluginManager
   */
  protected $pluginManager;
  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;
  protected $languageManager;
  /**
   * List of available stemmers that are not disabled.
   *
   * @var array
   */
  protected $enabledStemmers = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(AvalanchePluginManager $pluginManager, ConfigFactoryInterface $configFactory, LanguageManagerInterface $languageManager) {
    $this->pluginManager = $pluginManager;
    $this->configFactory = $configFactory;
    $this->languageManager = $languageManager;
    $this->enabledStemmers = $this->getEnabledStemmers();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.avalanche'),
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * Initializes and returns a stemmer.
   *
   * We only ever want one instance of each stemmer; the plugin manager will
   * always return the same instance of each plugin (keyed by $id).
   *
   * @param string $id
   *   ID of the stemmer class to create an instance of.
   * @param array $options
   *   Array of options to pass to the stemmer instance.
   *
   * @return Drupal\avalanche\StemmerInterface
   *   The initialized stemmer instance.
   */
  public function getStemmerInstance($id, array $options = []) {
    return $this->pluginManager->createInstance($id, $options);
  }

  /**
   * Gets a list of all supported languages from the plugin manager.
   *
   * @return array
   *   Array of language codes with the plugins that support them.
   */
  public function getSupportedLanguages() {
    return $this->pluginManager->getSupportedLanguages();
  }

  /**
   * Gets the ID of a stemmer for a specific language from the plugin manager.
   *
   * @param string $language
   *   The language (e.g. 'fi' or 'sv') to find a stemmer for.
   *
   * @return string
   *   ID of the stemmer for the given language (empty string if none exists).
   */
  public function getStemmersByLanguage($language) {
    return $this->pluginManager->getStemmersByLanguage($language);
  }

  /**
   * Separates enabled stemmer plugins out of a given list or all available.
   *
   * @param string|null $langcode
   *   (optional) Language code to get statuses for.
   * @param array|null $stemmers
   *   (optional) IDs of stemmers to get status for, or, by default, an empty
   *   array to get status of all available stemmers.
   *
   * @return array
   *   - List of plugin IDs of all enabled stemmers for the given $langcode if
   *   $langcode is given but $stemmers is not. One-dimensional, may be empty.
   *   - List of plugin IDs of all enabled stemmers out of the supplied list of
   *   $stemmers for the given language if both $stemmers and $langcode are
   *   given. One-dimensional, may be empty.
   *   - List of plugin IDs of all enabled stemmers out of the supplied list of
   *   $stemmers, keyed by language (two-dimensional), if only $stemmers is
   *   given. May be empty.
   *   - List of plugin IDs of all enabled stemmers in all supported languages
   *   if neither $stemmers nor $langcode is given. The list is keyed by
   *   language code and may be empty as a whole or for a specific language.
   *
   * @todo Should cache results until plugin configuration changes.
   */
  public function getEnabledStemmers($langcode = NULL, array $stemmers = []) {
    if (empty($this->enabledStemmers)) {
      $plugins = $this->pluginManager->getPlugins();

      // Global disable switch.
      $global = 'avalanche.settings';
      $globalDisabled = $this->configFactory->get($global)->get('disable');

      // Language-specific disable switches for each language.
      $languages = array_keys($this->pluginManager->getSupportedLanguages());
      $languageDisabled = array_fill_keys($languages, FALSE);
      foreach ($languages as $language) {
        $configName = $global . '.' . $language;
        if ($this->configFactory->get($configName)->get('disable')) {
          $languageDisabled[$language] = TRUE;
        }
      }

      $enabledStemmers = array_fill_keys($languages, []);

      foreach ($plugins as $id => $plugin) {
        foreach ($plugin['languages'] as $supportedLanguage) {
          if ($globalDisabled || $languageDisabled[$supportedLanguage]) {
            continue;
          }

          $configName = 'avalanche.settings.' . $supportedLanguage . '.' . $id;
          $disabled = $this->configFactory->get($configName)->get('disable');

          if (!$disabled) {
            $enabledStemmers[$supportedLanguage][] = $plugin['id'];
          }
        }
      }

      $this->enabledStemmers = $enabledStemmers;
    }

    // Check status of requested stemmer plugins for the requested language.
    if (!empty($stemmers) && !empty($langcode)) {
      foreach ($stemmers as $index => $stemmer) {
        // Avalanche has no stemmers for languages it doesn't recognize.
        if (empty($this->enabledStemmers[$langcode])) {
          $stemmers[$index] = NULL;
          continue;
        }

        $enabled = array_search($stemmer, $this->enabledStemmers[$langcode]);
        if ($enabled === FALSE) {
          $stemmers[$index] = NULL;
        }
      }

      return array_unique(array_filter($stemmers));
    }
    // Check status of all stemmer plugins for the requested language.
    elseif (empty($stemmers) && !empty($langcode)) {
      // Avalanche has no stemmers for languages it doesn't recognize.
      if (!isset($this->enabledStemmers[$langcode])) {
        return [];
      }

      return $this->enabledStemmers[$langcode];
    }
    // Check status of requested stemmer plugins for all languages.
    elseif (!empty($stemmers) && empty($langcode)) {
      $enabled = $this->enabledStemmers;

      foreach ($this->enabledStemmers as $language => $stemmerList) {
        foreach ($stemmerList as $index => $stemmer) {
          if (!in_array($stemmer, $stemmers)) {
            unset($enabled[$language][$index]);
          }
        }
        // Reindex the array.
        $enabled[$language] = array_values($enabled[$language]);
      }

      return array_filter($enabled);
    }

    // Check status of all stemmer plugins for all languages.
    return $this->enabledStemmers;
  }

  /**
   * Gets the enabled/disabled status of one or more stemmer plugins.
   *
   * @param string|array $stemmer
   *   ID (string) or IDs (array of strings) of stemmers to get the status for.
   *   An array with a single string value is treated as a string.
   * @param string|null $langcode
   *   (optional) Language code for which to check a status.
   *
   * @return bool|array
   *   - If $stemmer is a string and $langcode is given, returns TRUE if that
   *   stemmer is enabled for the given language and FALSE if not.
   *   - If $stemmer is an array and $langcode is given, returns an array of
   *   booleans keyed by stemmer ID with the above logic (TRUE if enabled).
   *   - If $stemmer is a string or an array and $langcode is not given,
   *   returns an array keyed by all supported language codes. Under each
   *   language code will be an array keyed by the given stemmer IDs with a
   *   value of TRUE or FALSE to denote whether that stemmer is enabled for
   *   that language.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the $stemmer parameter is not a string or an array.
   */
  public function getStemmerStatus($stemmer, $langcode = NULL) {
    if (is_string($stemmer) || (is_array($stemmer) && count($stemmer) == 1)) {
      if (is_array($stemmer)) {
        $stemmer = array_shift($stemmer);
      }

      $status = $this->getEnabledStemmers($langcode, [$stemmer]);

      // If $langcode is empty, the return value will be an array.
      if (empty($langcode)) {
        $enabled = array_fill_keys(
          array_keys($this->pluginManager->getSupportedLanguages()),
          [$stemmer => FALSE]
        );

        foreach (array_keys($enabled) as $langcode) {
          if (!empty($status[$langcode])) {
            $enabled[$langcode][$stemmer] = TRUE;
          }
        }
      }
      else {
        $enabled = !empty($status);
      }
    }
    elseif (is_array($stemmer)) {
      $status = $this->getEnabledStemmers($langcode, $stemmer);

      // If $langcode is empty, the return value will be an array.
      if (empty($langcode)) {
        $enabled = array_fill_keys(
          array_keys($this->pluginManager->getSupportedLanguages()),
          array_fill_keys($stemmer, FALSE)
        );

        foreach ($status as $langcode => $stemmers) {
          foreach ($stemmer as $id) {
            if (in_array($id, $stemmers)) {
              $enabled[$langcode][$id] = TRUE;
            }
          }
        }
      }
      else {
        $enabled = array_fill_keys($stemmer, FALSE);

        foreach ($stemmer as $id) {
          if (in_array($id, $status)) {
            $enabled[$id] = TRUE;
          }
        }
      }
    }
    else {
      throw new \InvalidArgumentException(
        sprintf(
          "Argument '$stemmer' must be a string or an array, %s given.",
          gettype($stemmer)
        )
      );
    }

    return $enabled;
  }

  /**
   * Gets the title of a language's configuration page.
   *
   * @param string $language
   *   The ID of the language for which to get the configuration page title.
   *
   * @return string
   *   The title of the language's configuration page.
   */
  public function getLanguageSettingsFormTitle($language) {
    $language_name = $this->languageManager->getLanguageName($language);

    return $this->t(
        'Configure Avalanche: @name',
        [
          '@name' => $language_name,
        ]
    );
  }

  /**
   * Gets the title of a stemmer's configuration page.
   *
   * @param string $stemmer
   *   The ID of the stemmer for which to get the configuration page title.
   *
   * @return string
   *   The title of the stemmer's configuration page.
   */
  public function getStemmerSettingsFormTitle($stemmer) {
    $instance = $this->getStemmerInstance($stemmer);
    $definition = $instance->getPluginDefinition();

    return $this->t(
        'Configure Avalanche: @name',
        [
          '@name' => $definition['title'],
        ]
    );
  }

}
