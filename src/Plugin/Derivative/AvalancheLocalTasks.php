<?php

namespace Drupal\avalanche\Plugin\Derivative;

use Drupal\avalanche\AvalanchePluginManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates local task links for each stemmer plugin.
 */
class AvalancheLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  protected $basePluginId;
  protected $pluginManager;
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct($base_plugin_id, AvalanchePluginManager $pluginManager, LanguageManagerInterface $languageManager) {
    $this->basePluginId = $base_plugin_id;
    $this->pluginManager = $pluginManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
        $base_plugin_id,
        $container->get('plugin.manager.avalanche'),
        $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    $languages = $this->languageManager->getLanguages();
    $stemmable = $this->pluginManager->getSupportedLanguages();

    $items = array_intersect(
      array_keys($languages),
      array_keys($stemmable)
    );

    foreach ($items as $langcode) {
      $parent = "avalanche.settings.stemmers:avalanche.settings.$langcode";

      $this->derivatives["avalanche.settings.$langcode"] = [
        'title' => $languages[$langcode]->getName(),
        'base_route' => 'avalanche.settings',
        'route_name' => "avalanche.settings.$langcode",
      ];

      $this->derivatives["avalanche.settings.$langcode.default"] = [
        'title' => 'Overview',
        'route_name' => "avalanche.settings.$langcode",
        'parent_id' => $parent,
      ];

      foreach ($stemmable[$langcode] as $stemmer) {
        $this->derivatives["avalanche.settings.$langcode.$stemmer"] = [
          'title' => $stemmer,
          'route_name' => "avalanche.settings.$langcode.$stemmer",
          'parent_id' => $parent,
        ];
      }

    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
