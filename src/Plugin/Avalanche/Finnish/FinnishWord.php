<?php

namespace Drupal\avalanche\Plugin\Avalanche\Finnish;

use Drupal\avalanche\Token;
use Drupal\avalanche\NormalizationInterface;
use Drupal\Component\Utility\Unicode;

/**
 * Finnish token (word) class.
 *
 * Stores data about a single word, its regions and stem. Debug information on
 * the stemming process of the word is also stored, though not displayed by
 * default.
 */
class FinnishWord extends Token implements NormalizationInterface {

  /**
   * Vowels in regex character set notation.
   *
   * Finnish vowels are a, e, i, o, u, y, ä and ö. Other vowels, such as é or
   * ü, may get treated as consonants (see explanation below). Normalization
   * can help alleviate this problem.
   *
   * @var string
   */
  private $vowel = '[aeiouyäö]';
  /**
   * Restricted vowels (all except y), in regex character set notation.
   *
   * @var string
   */
  private $restrictedVowel = '[aeiouäö]';
  /**
   * Two consecutive restricted vowels, in regex character set notation.
   *
   * @var string
   */
  private $longVowel = '(aa|ee|ii|oo|uu|ää|öö)';
  /**
   * Non-vowels in regex notation, i.e. anything that's not a vowel.
   *
   * There are 13 consonants that appear in Finnish words ([dghjklmnprstv]),
   * but several more appear in words of foreign origin (e.g. [bcfqwxz]).
   * It's difficult to make a comprehensive list, and for the purposes of
   * stemming, any non-vowel is treated as a consonant. Check out normalization
   * to get better results with non-vowels.
   *
   * @var string
   */
  private $consonant = '[^aeiouyäö]';

  /**
   * Initializes a FinnishWord instance.
   *
   * @param string $token
   *   The word to be stemmed.
   */
  public function __construct($token) {
    // Store the original word in $text and also set the given input as the
    // initial value of $stem - we don't yet know if it'll get stemmed at all,
    // as it might not have suitable regions to process.
    $this->word = $token;
    $this->stem = $token;

    // Set the named patterns found in regular expressions for Finnish.
    $this->namedPatterns = [
      // Vowels.
      '<%V%>' => $this->vowel,
      // Restricted vowels.
      '<%RV%>' => $this->restrictedVowel,
      // Long vowels.
      '<%LV%>' => $this->longVowel,
      // Consonants.
      '<%C%>' => $this->consonant,
    ];

    // Class implements Normalization; accept a second parameter to determine
    // level of normalization and normalize the input word.
    if ($this instanceof NormalizationInterface) {
      // At minimum, the word is trimmed to eliminate useless whitespace and
      // some common punctuation. Note that while the Snowball introduction at
      // [1] mentions that the Porter stemmer also normalizes words by removing
      // accents from letter-accent combinations that do not form part of the
      // alphabet of the language, the Finnish stemmer documentation at [2] does
      // not describe this process. The test output provided also includes
      // foreign accented characters (e.g. é). Thus, the normalization beyond
      // trimming the word is entirely experimental.
      //
      // @see http://snowballstem.org/texts/introduction.html [1]
      // @see http://snowballstem.org/algorithms/finnish/stemmer.html [2]
      $this->stem = self::normalize($this->stem, FALSE);
    }

    // Determine the starting byte offset of region 1.
    $this->r1 = $this->getRegion();
    // Determine the starting byte offset of region 2.
    $this->r2 = $this->getRegion($this->r1);
  }

  /**
   * Normalizes a given token by e.g. converting it into lowercase.
   *
   * At minimum, converts the token into lowercase and removes any whitespace
   * around it. Does not touch punctuation (e.g. hyphens); those should have
   * been handled by the stemmer's tokenization process.
   *
   * If $convert_chars is TRUE, also attempts to convert foreign letters (such
   * as many with diacritics) that may appear in Finnish texts but not in words
   * of Finnish origin (say, é in Lindén). These will be changed so that they
   * get treated like their non-accented counterparts. In many cases, this is
   * most likely wrong considering the language the letter originates from.
   * It may be preferable to transliteration, however, as that often goes too
   * far (a good example is replacing ä with ae - hän 's/he' does *not* equal
   * haen "I'll fetch").
   *
   * Not complete or perfect by any stretch of the imagination. Will butcher
   * some languages, but then again, this is a class for Finnish words.
   *
   * Based on https://www.cs.tut.fi/~jkorpela/lang/finnish-letters.html and
   * https://en.wikipedia.org/wiki/Finnish_orthography and a good bit of good
   * old guessing AKA "That thing kind of looks like an s. Let's make it an s."
   *
   * @param string $token
   *   The token (word) to be normalized.
   * @param bool $convert_chars
   *   (optional) TRUE if foreign characters should be converted to local ones,
   *   FALSE if not. Experimental, but potentially useful feature.
   *   Default: FALSE.
   *
   * @return string
   *   The normalized token.
   *
   * @todo Character conversion needs testing (and separate unit tests).
   */
  public static function normalize($token, $convert_chars = FALSE) {
    // An array of characters or character sequences to replace with another
    // character or character sequence during normalization. Make sure that
    // replacements don't match any replace patterns, as str_replace is not
    // capable of dealing with it. Change the foreach loop below into a strtr
    // call below if you need this functionality, but note that it's slower.
    static $replace_pairs = [
      'š' => 'sh',
      'ž' => 'zh',
      // German.
      'ß' => 'ss',
      // Icelandic.
      'þ' => 'th',
      // Pronunciation-wise would be 'ä' (Danish, Norwegian).
      'æ' => 'ae',
      'á' => 'a',
      'à' => 'a',
      'ã' => 'a',
      'â' => 'a',
      'é' => 'e',
      'è' => 'e',
      'ë' => 'e',
      'ê' => 'e',
      'í' => 'i',
      'ì' => 'i',
      'î' => 'i',
      'ï' => 'i',
      // Pronunciation-wise would be 'y' (German, Estonian).
      'ü' => 'u',
      'ú' => 'u',
      'ù' => 'u',
      'û' => 'u',
      'ý' => 'y',
      'ÿ' => 'y',
      // Spanish.
      'ñ' => 'n',
      // French.
      'ç' => 'c',
      // Pronunciation (Norwegian).
      'ø' => 'ö',
      // Common usage (Estonian).
      'õ' => 'o',
      'ó' => 'o',
      'ò' => 'o',
      'ô' => 'o',
      'č' => 'c',
      'đ' => 'd',
      'ŋ' => 'n',
      'ŧ' => 't',
      'ʒ' => 'z',
      'ǥ' => 'g',
      'ǧ' => 'g',
      'ǩ' => 'k',
      'ǯ' => 'z',
      'ȟ' => 'h',
      // Icelandic.
      'ð' => 'd',
    ];

    // Trim excess whitespace and lowercase the token.
    $token = Unicode::strtolower(trim($token));

    if ($convert_chars) {
      // A foreach loop with str_translate is much faster than a single strtr.
      foreach ($replace_pairs as $key => $value) {
        $token = str_replace($key, $value, $token);
      }
    }

    return $token;
  }

  /**
   * Determines the regions as they are defined in the Snowball documentation.
   *
   * [quote from http://snowballstem.org/texts/r1r2.html]
   *  R1 is the region after the first non-vowel following a vowel, or is the
   *  null region at the end of the word if there is no such non-vowel.
   *
   *  R2 is the region after the first non-vowel following a vowel in R1, or is
   *  the null region at the end of the word if there is no such non-vowel.
   * [/quote]
   *
   * @param int $offset
   *   Byte offset at which to start looking for a region.
   *
   * @return int
   *   Region's starting byte (NOT char) offset; can also be 0 or last byte.
   */
  protected function getRegion($offset = 0) {
    // Array for holding matches (see preg_match documentation).
    $matches = [];
    // Pattern for finding R1 and R2. (?<=foo)bar matches bar when preceded
    // by foo, regardless of whether the preceding string is inside $offset.
    // Notice the use of the /u switch - with Finnish, we deal with multibyte
    // characters much of the time.
    $pattern = '/(?<=' . $this->vowel . ')' . $this->consonant . '(.+)/u';

    // If there is no word to determine a region for, we can simply return NULL.
    // If a word has no regions, it won't get stemmed at all.
    if (empty($this->stem)) {
      return NULL;
    }

    // If $offset is NULL, make it an integer instead.
    $offset = empty($offset) ? 0 : $offset;

    // Find the next region, starting from byte offset $offset.
    if (preg_match($pattern, $this->stem, $matches, PREG_OFFSET_CAPTURE, $offset)) {
      // If a region is found, return its starting byte offset.
      return $matches[1][1];
    }

    // If a region cannot be found, return the length of the string in bytes.
    // N.B. This will be larger than the character length in multibyte strings.
    // Using the byte length here is fully intentional.
    return strlen($this->stem);
  }

}
