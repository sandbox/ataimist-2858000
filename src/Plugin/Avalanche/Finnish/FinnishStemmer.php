<?php

namespace Drupal\avalanche\Plugin\Avalanche\Finnish;

use Drupal\avalanche\StemmerBase;
use Drupal\avalanche\TokenizationInterface;
use Drupal\avalanche\StopwordInterface;

/**
 * Finnish Snowball stemmer.
 *
 * Stems Finnish words using the Snowball stemmer algorithm described at the
 * Snowball website. Additionally implements tokenization and stopwords, both
 * of which are experimental.
 *
 * @see http://snowballstem.org/algorithms/finnish/stemmer.html
 * @see Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord
 *
 * @Stemmer(
 *   id = "FinnishStemmer",
 *   languages = {
 *     "fi",
 *     "fi-FI",
 *     "fi-SE",
 *   },
 *   title = "Finnish"
 * )
 */
class FinnishStemmer extends StemmerBase implements TokenizationInterface, StopwordInterface {

  /**
   * Default array of Finnish stopwords that are exempt from stemming.
   *
   * Stopwords that shouldn't be stemmed. There are some intentional duplicates
   * to keep the list complete. For example, 'sinä' is 'you' and the essive
   * form of 'it', so it appears twice. The list originates from [1] and has
   * been converted into PHP array format to make using it easier and faster.
   *
   * @var array
   *
   * @see http://snowballstem.org/algorithms/finnish/stemmer.html [1]
   *
   * @todo Stopwords need to be configurable.
   */
  private static $stopwords = [
    // Forms of the verb '(to) be'.
    'olla', 'olen', 'olet', 'on', 'olemme', 'olette', 'ovat', 'ole',
    'oli', 'olisi', 'olisit', 'olisin', 'olisimme', 'olisitte', 'olisivat',
    'olit', 'olin', 'olimme', 'olitte', 'olivat', 'ollut', 'olleet',
    // Negation.
    'en', 'et', 'ei', 'emme', 'ette', 'eivät',
    // Pronouns.
    'minä', 'minun', 'minut', 'minua', 'minussa', 'minusta', 'minuun',
    'minulla', 'minulta', 'minulle',
    'sinä', 'sinun', 'sinut', 'sinua', 'sinussa', 'sinusta', 'sinuun',
    'sinulla', 'sinulta', 'sinulle',
    'hän', 'hänen', 'hänet', 'häntä', 'hänessä', 'hänestä', 'häneen',
    'hänellä', 'häneltä', 'hänelle',
    'me', 'meidän', 'meidät', 'meitä', 'meissä', 'meistä', 'meihin',
    'meillä', 'meiltä', 'meille',
    'te', 'teidän', 'teidät', 'teitä', 'teissä', 'teistä', 'teihin',
    'teillä', 'teiltä', 'teille',
    'he', 'heidän', 'heidät', 'heitä', 'heissä', 'heistä', 'heihin',
    'heillä', 'heiltä', 'heille',
    'tämä', 'tämän', 'tätä', 'tässä', 'tästä', 'tähän', 'tällä', 'tältä',
    'tälle', 'tänä', 'täksi',
    'tuo', 'tuon', 'tuota', 'tuossa', 'tuosta', 'tuohon', 'tuolla', 'tuolta',
    'tuolle', 'tuona', 'tuoksi',
    'se', 'sen', 'sitä', 'siinä', 'siitä', 'siihen', 'sillä', 'siltä',
    'sille', 'sinä', 'siksi',
    'nämä', 'noiden', 'näitä', 'näissä', 'näistä', 'näihin', 'näillä', 'näiltä',
    'näille', 'näinä', 'näiksi',
    'nuo', 'noiden', 'noita', 'noissa', 'noista', 'noihin', 'noilla', 'noilta',
    'noille', 'noina', 'noiksi',
    'ne', 'niiden', 'niitä', 'niissä', 'niistä', 'niihin', 'niillä', 'niiltä',
    'niille', 'niinä', 'niiksi',
    'kuka', 'kenen', 'kenet', 'ketä', 'kenessä', 'kenestä', 'keneen', 'kenellä',
    'keneltä', 'kenelle', 'kenenä', 'keneksi',
    'ketkä', 'keiden', 'ketkä', 'keitä', 'keissä', 'keistä', 'keihin', 'keillä',
    'keiltä', 'keille', 'keinä', 'keiksi',
    'mikä', 'minkä', 'mitkä', 'mitä', 'missä', 'mistä', 'mihin', 'millä',
    'miltä', 'mille', 'minä', 'miksi',
    'mitkä',
    'joka', 'jonka', 'jota', 'jossa', 'josta', 'johon', 'jolla', 'jolta',
    'jolle', 'jona', 'joksi',
    'jotka', 'joiden', 'joita', 'joissa', 'joista', 'joihin', 'joilla',
    'joilta', 'joille', 'joina', 'joiksi',
    // Conjunctions.
    'että', 'ja', 'jos', 'koska', 'kuin', 'mutta', 'niin', 'sekä', 'sillä',
    'tai', 'vaan', 'vai', 'vaikka',
    // Prepositions.
    'kanssa', 'mukaan', 'noin', 'poikki', 'yli',
    // Other.
    'kun', 'niin', 'nyt', 'itse',
  ];

  /**
   * Stems the given token.
   *
   * Goes through the stemming steps in the order defined by the Snowball
   * documentation.
   *
   * @param string $token
   *   The word to be stemmed.
   * @param string|null $langcode
   *   (optional) The language the word is in. Optional as StemmerInterface
   *   dictates, not used with this stemmer.
   *
   * @return \Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord
   *   The full token object; use ->getStem() to get the stemming result.
   */
  public function stem($token, $langcode = NULL) {
    $this->word = new FinnishWord($token);

    // Stemming is only done if the word has regions.
    if ($this->word->getRegions()) {
      $this->stepParticlesAdverbs($this->word);
      $this->stepPossessives($this->word);
      $this->stepCases($this->word);
      $this->stepEndings($this->word);
      $this->stepPlurals($this->word);
      $this->stepCleanup($this->word);
    }

    return $this->word;
  }

  /**
   * Determines whether a given token is a stopword.
   *
   * Matches a word against the predefined stopword list. As a static function,
   * can be called without an existing FinnishStemmer instance.
   *
   * @param string $token
   *   The word to be checked.
   *
   * @return bool
   *   TRUE if the word is a stopword, FALSE if it isn't.
   *
   * @todo Could (should) be moved into a trait or base class.
   */
  public static function matchStopword($token) {
    static $tokens = [];

    // Normalize the token first, but don't do character replacements.
    $token = FinnishWord::normalize($token, FALSE);

    // If we've already checked this token, let's not do it again.
    if (isset($tokens[$token])) {
      return $tokens[$token];
    }

    $tokens[$token] = in_array($token, self::$stopwords);

    return $tokens[$token];
  }

  /**
   * Breaks a string down into one or more tokens.
   *
   * Tokenizes the input string by breaking it down into one or more words; for
   * example, the compound word linja-auto ('bus') produces two tokens, linja
   * ('route') and auto ('car'). Some tokens can be discarded, e.g. because they
   * are not useful on their own (such as yli 'super' or ali 'sub' in words like
   * yli-inhimillinen 'superhuman'). This is experimental and only happens when
   * the original string is a compound word.
   *
   * You can customize the produced tokens by supplying your own array of words
   * to discard ($discard_tokens). If you do so, note that prefixing a word in
   * the array with an asterisk causes only the specified case to be matched.
   * This is because the case of the first letter often has special meaning.
   *
   * You can also specify a minimum length for the produced tokens with the
   * $min_length parameter. The default minimum length is 3, so that useless
   * prefixes like 'ex' (ex-vaimo 'ex-wife') or 'A' (A-luokka 'class A') won't
   * be returned.
   *
   * @param string $string
   *   The string to be tokenized.
   * @param int $min_length
   *   (optional) Minimum length of returned token. Default: 3.
   * @param array $discard_tokens
   *   (optional) Array of tokens to be discarded.
   *
   * @return array
   *   An array of words extracted from the input string. Can be empty.
   *
   * @todo Doesn't actually do tokenization yet.
   */
  public static function tokenize($string, $min_length = 3, array $discard_tokens = []) {

    if (empty($discard_tokens)) {
      // These are primarily the first parts of compound words where the first
      // part ends with the same vowel as the next part starts with, which
      // requires that a hyphen is inserted between the two parts. The list is
      // meant to contain words that make no sense as tokens, since they mean
      // nothing relevant on their own - that is, prefixes and suffixes that
      // modify the actual word. When they appear alone (e.g. 'euro' to denote
      // the currency) they get stemmed normally.
      $discard_tokens = [
        // Sub- (Ala-Saksi 'Lower Saxony', ala-arvoinen 'poor').
        'ala',
        // Sub- (ali-ihminen 'subhuman').
        'ali',
        // Open (avo-osasto 'open ward').
        'avo',
        // Extra (ekstra-alennus 'extra discount', Finnish spelling).
        'ekstra',
        // Functional entity (puolue-elin 'party organ').
        'elin',
        // Non- (epä-älyllinen 'unintelligent').
        'epä',
        // Pre-, fore- or preceding (esi-isä 'forefather').
        'esi',
        // Far, remote (etä-äiti, a mother who isn't the primary caretaker).
        'etä',
        // Relating to euro or Europe (euro-optimistinen 'euro-optimistic').
        'euro',
        // Extra (extra-alennus 'extra discount', English spelling).
        'extra',
        // Scattered (haja-asutus 'scattered settlement').
        'haja',
        // Detached, detachable (irto-olkaimet 'detachable straps').
        'irto',
        // Perpetual, 'very' (iki-ihana 'very, very lovely').
        'iki',
        // Self- (unlikely; itse-eronnut 'self-resigned').
        'itse',
        // Far, remote (kauko-ohjaus 'remote control'). Only if not capital K.
        '*kauko',
        // Middle- (keski-ikäinen 'middle-aged').
        'keski',
        // Extra, added (lisä-ääni 'extra sound').
        'lisä',
        // Near (lähi-ihminen ~'support person'). Only if not capital L.
        '*lähi',
        // West (länsi-intialainen 'West Indian'). Only if not capital L.
        '*länsi',
        // Macro- (makro-objektiivi 'macro lens').
        'makro',
        // Mega- (unlikely; mega-arkinen 'mind-numbingly ordinary').
        'mega',
        // Meta- (meta-arviointi 'evaluation of evaluation').
        'meta',
        // Micro- (mikro-organismi 'microorganism').
        'mikro',
        // Multi-, poly- (moni-ilmeinen, having many looks or expressions).
        'moni',
        // Current, 'of now' (nyky-yhteiskunta 'today's society').
        'nyky',
        // Self- (oma-alotteinen 'self-imposed').
        'oma',
        // N-piece; relating to status (kolmiosainen 'three-piece',
        // huono-osainen 'disadvantaged'), from osa 'part'.
        'osainen',
        // Relating to status (huono-osaisuus, being disadvantaged).
        'osaisuus',
        // Quick (pika-ateria 'quick meal').
        'pika',
        // Semi- (semi-intellektuelli 'semi-intellectual').
        'semi',
        // Inner (sisä-ääni 'inside sound').
        'sisä',
        // Rear (taka-akseli 'rear axle').
        'taka',
        // Equal (tasa-arvo 'equality').
        'tasa',
        // Outer (ulko-ovi 'outer [front] door').
        'ulko',
        // Ultra-, extremely (unlikely; ultra-askeettinen 'extremely ascetic').
        'ultra',
        // Anti- (vasta-aine 'antibody', vasta-alkaja 'beginner').
        'vasta',
        // Super- (yli-ihminen 'superhuman').
        'yli',
        // Extremely, excessively (unlikely; yltiö-öljyinen 'excessively oily').
        'yltiö',
        // Super- (Ylä-Savo 'Upper Savonia', ylä-ääni 'overtone').
        'ylä',
        // Quick, unexpected (äkki-ihastuminen 'sudden infatuation').
        'äkki',
      ];
    }

    return $string;
  }

  /**
   * Performs step 1 of the Snowball stemming.
   *
   * [quote]
   *  Search for the longest among the following suffixes in R1, and perform
   *  the action indicated:
   *
   *  (a) kin kaan kään ko kö han hän pa pä:
   *    delete if preceded by n, t or a vowel
   *  (b) sti:
   *    delete if in R2
   * [/quote]
   *
   * @param Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord $word
   *   The FinnishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/finnish/stemmer.html
   */
  private function stepParticlesAdverbs(FinnishWord &$word) {
    $this->setStep(1);

    // Suffix length: 4
    // kaan kään: delete if in R1 and preceded by n, t or a vowel.
    if ($word->hasPattern('/(?<=(n|t|<%V%>))(kaan|kään)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-4);
    }
    // Suffix length: 3
    // kin han hän: delete if in R1 and preceded by n, t or a vowel.
    elseif ($word->hasPattern('/(?<=(n|t|<%V%>))(kin|han|hän)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-3);
    }
    // Suffix length: 3
    // sti: delete if in R2.
    elseif ($word->hasPattern('/sti$/u', AVALANCHE_R2)) {
      $word->deleteSuffix(-3);
    }
    // Suffix length: 2
    // ko kö pa pä: delete if in R1 and preceded by n, t or a vowel.
    elseif ($word->hasPattern('/(?<=(n|t|<%V%>))(ko|kö|pa|pä)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-2);
    }
  }

  /**
   * Performs step 2 of the Snowball stemming.
   *
   * [quote]
   *  Search for the longest among the following suffixes in R1, and perform
   *  the action indicated:
   *
   *  si: delete if not preceded by k
   *  ni: delete; if preceded by kse, replace with ksi
   *  nsa nsä mme nne: delete
   *  an: delete if preceded by one of ta ssa sta lla lta na
   *  än: delete if preceded by one of tä ssä stä llä ltä nä
   *  en: delete if preceded by one of lle ine
   * [/quote]
   *
   * @param Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord $word
   *   The FinnishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/finnish/stemmer.html
   */
  private function stepPossessives(FinnishWord &$word) {
    $this->setStep(2);

    // Suffix length: 3
    // nsa nsä mme nne: delete.
    if ($word->hasPattern('/(nsa|nsä|mme|nne)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-3);
    }
    // Suffix length: 2
    // si: delete if not preceded by k.
    elseif ($matches = $word->hasPattern('/(?<=(.))si$/u', AVALANCHE_R1)) {
      if ($matches[1][0] != 'k') {
        $word->deleteSuffix(-2);
      }
    }
    // Suffix length: 2
    // ni: delete; if preceded by kse, replace with ksi.
    elseif ($word->hasPattern('/(ni)$/u', AVALANCHE_R1)) {
      if ($word->hasPattern('/(?<=kse)ni$/u', AVALANCHE_R1)) {
        $word->deleteSuffix(-3);
        $word->insertSuffix('i');
      }
      else {
        $word->deleteSuffix(-2);
      }
    }
    // Suffix length: 2
    // en: delete if preceded by one of lle ine.
    elseif ($word->hasPattern('/(?<=lle|ine)en$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-2);
    }
    // Suffix length: 2
    // an: delete if preceded by one of ta ssa sta lla lta na.
    // än: delete if preceded by one of tä ssä stä llä ltä nä.
    elseif ($word->hasPattern('/((?<=ssa|sta|lla|lta)an|(?<=ta|na)an|(?<=ssä|stä|llä|ltä)än|(?<=tä|nä)än)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-2);
    }
  }

  /**
   * Performs step 3 of the Snowball stemming.
   *
   * [quote]
   *  Search for the longest among the following suffixes in R1, and perform
   *  the action indicated:
   *
   *  hXn preceded by X, where X is a V other than u (a/han, e/hen etc): delete
   *  siin den tten, preceded by Vi: delete
   *  seen, preceded by LV: delete
   *  a ä, preceded by cv: delete
   *  tta ttä, preceded by e: delete
   *  ta tä ssa ssä sta stä lla llä lta ltä lle na nä ksi ine: delete
   *  n: delete; if preceded by LV or ie, delete the last vowel
   * [/quote]
   *
   * @param Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord $word
   *   The FinnishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/finnish/stemmer.html
   *
   * @todo
   *   Check results that match 'hXn preceded by X'; it would seem that doing
   *   this check differently than described in the documentation would make
   *   more sense (generally resulting in -n getting removed instead of -hXn,
   *   and the remaining -hX getting removed in step 6b). See the comment
   *   inside the function.
   */
  private function stepCases(FinnishWord &$word) {
    $this->setStep(3);

    // Suffix length: 4
    // siin tten, preceded by Vi: delete.
    // seen, preceded by LV: delete.
    if ($word->hasPattern('/(((?<=<%LV%>)seen)|((?<=<%RV%>i)(siin|tten)))$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-4);
    }
    // Suffix length: 3
    // hXn preceded by X, where X is a V other than u (e.g. a/han): delete.
    //
    // @todo
    //   The documentation seems incorrect here; in order to match the Finnish
    //   stemmer demo on the Snowball web site and get expected results with
    //   the test data, we must first match a hXn suffix (where X is as
    //   described), then delete it if it is preceded by X. Even an initial
    //   match without the subsequent deletion, however, must still cause the
    //   rest of step 3 to be skipped (which seems contrary to the
    //   documentation). Otherwise, words with -han, -hen &c. suffixes would
    //   incorrectly get their final -n deleted in this step and many also the
    //   final vowel in step 6b. Thus, while /(?<=([aeioäö]))h\1n$/u would
    //   perfectly match the described pattern, we must instead do this check
    //   in two steps.
    elseif ($matches = $word->hasPattern('/h([aeioäö])n$/u', AVALANCHE_R1)) {
      if (!empty($matches[1])) {
        if ($word->hasPattern('/(?<=' . $matches[1][0] . ')h' . $matches[1][0] . 'n$/u', AVALANCHE_R1)) {
          $word->deleteSuffix(-3);
        }
      }
    }
    // Suffix length: 3
    // ssa ssä sta stä lla llä lta ltä lle ksi ine: delete.
    // den, preceded by Vi: delete.
    // tta ttä, preceded by e: delete.
    elseif ($word->hasPattern('/(((?<=<%RV%>i)den)|((?<=e)tt(a|ä))|ssa|ssä|sta|stä|lla|llä|lta|ltä|lle|ksi|ine)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-3);
    }
    // Suffix length: 2
    // ta tä na nä: delete.
    elseif ($word->hasPattern('/(ta|tä|na|nä)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-2);
    }
    // Suffix length: 1
    // n: delete; if preceded by LV or ie, [also] delete the last vowel.
    elseif ($word->hasPattern('/n$/u', AVALANCHE_R1)) {
      if ($word->hasPattern('/(?<=(<%LV%>|ie))n$/u', AVALANCHE_R1)) {
        $word->deleteSuffix(-2);
      }
      else {
        $word->deleteSuffix(-1);
      }
    }
    // Suffix length: 1
    // a ä, preceded by cv: delete.
    elseif ($word->hasPattern('/(?<=(<%C%><%V%>))(a|ä)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }
  }

  /**
   * Performs step 4 of the Snowball stemming.
   *
   * [quote]
   *  Search for the longest among the following suffixes in R2, and perform
   *  the action indicated:
   *
   *  mpi mpa mpä mmi mma mmä: delete if not preceded by po
   *  impi impa impä immi imma immä eja ejä: delete
   * [/quote]
   *
   * @param Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord $word
   *   The FinnishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/finnish/stemmer.html
   */
  private function stepEndings(FinnishWord &$word) {
    $this->setStep(4);

    // Suffix length: 4
    // impi impa impä immi imma immä: delete.
    if ($word->hasPattern('/(impi|impa|impä|immi|imma|immä)$/u', AVALANCHE_R2)) {
      $word->deleteSuffix(-4);
    }
    // Suffix length: 3
    // eja ejä: delete.
    elseif ($word->hasPattern('/(eja|ejä)$/u', AVALANCHE_R2)) {
      $word->deleteSuffix(-3);
    }
    // Suffix length: 3
    // mpi mpa mpä mmi mma mmä: delete if not preceded by po.
    elseif ($word->hasPattern('/(?<!po)(mpi|mpa|mpä|mmi|mma|mmä)$/u', AVALANCHE_R2)) {
      $word->deleteSuffix(-3);
    }
  }

  /**
   * Performs step 5 of the Snowball stemming.
   *
   * [quote]
   *  If an ending was removed in step 3, delete a final i or j if in R1;
   *  otherwise, if an ending was not removed in step 3, delete a final t in R1
   *  if it follows a vowel, and, if a t is removed, delete a final mma or imma
   *  in R2, unless the mma is preceded by po.
   * [/quote]
   *
   * @param Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord $word
   *   The FinnishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/finnish/stemmer.html
   */
  private function stepPlurals(FinnishWord &$word) {
    $this->setStep(5);

    // If an ending was removed in step 3...
    if ($this->getStep(3) != $this->getStep(4)) {
      // ...delete a final i or j if in R1.
      if ($word->hasPattern('/(i|j)$/u', AVALANCHE_R1)) {
        $word->deleteSuffix(-1);
      }
      // Intentional end of function execution.
      return;
    }

    // Otherwise, if an ending was not removed in step 3, delete a final 't' in
    // R1 if it follows a vowel...
    if ($word->hasPattern('/(?<=<%V%>)t$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
      // ...and, if a 't' was removed, delete a final mma or imma in R2...
      if ($word->hasPattern('/imma$/u', AVALANCHE_R2)) {
        $word->deleteSuffix(-4);
      }
      // ...unless the mma in R2 is preceded by 'po'.
      elseif ($word->hasPattern('/(?<!po)mma$/u', AVALANCHE_R2)) {
        $word->deleteSuffix(-3);
      }
    }
  }

  /**
   * Performs step 6 of the Snowball stemming.
   *
   * [quote]
   *  Do in turn steps (a), (b), (c), (d), restricting all tests to the
   *  region R1.
   *  a) If R1 ends LV delete the last letter
   *  b) If R1 ends cX, c a consonant and X one of a ä e i, delete the last
   *     letter
   *  c) If R1 ends oj or uj delete the last letter
   *  d) If R1 ends jo delete the last letter.
   *
   *  Do step (e), which is not restricted to R1.
   *  e) If the word ends with a double consonant followed by zero or more
   *     vowels, remove the last consonant (so eläkk -> eläk, aatonaatto ->
   *     aatonaato).
   * [/quote]
   *
   * @param Drupal\avalanche\Plugin\Avalanche\Finnish\FinnishWord $word
   *   The FinnishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/finnish/stemmer.html
   */
  private function stepCleanup(FinnishWord &$word) {
    // Step 6a
    // If R1 ends LV delete the last letter.
    $this->setStep('6a');
    if ($word->hasPattern('/<%LV%>$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }

    // Step 6b
    // If R1 ends cX, c a consonant and X one of aäei, delete the last letter.
    $this->setStep('6b');
    if ($word->hasPattern('/<%C%>[aäei]$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }

    // Step 6c
    // If R1 ends oj or uj delete the last letter.
    $this->setStep('6c');
    if ($word->hasPattern('/(oj|uj)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }

    // Step 6d
    // If R1 ends jo delete the last letter.
    $this->setStep('6d');
    if ($word->hasPattern('/jo$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }

    // Step 6e
    // If the word ends with a double consonant followed by zero or more
    // vowels, remove the last consonant.
    $this->setStep('6e');
    if ($matches = $word->hasPattern('/((?P<consonant><%C%>)\k<consonant>)<%V%>*$/u')) {
      // Find the byte offset of the matched consonant cluster.
      $offset = strlen(utf8_decode(substr($word->getStem(), 0, $matches[0][1])));
      $word->deleteSuffix($offset, 1);
    }
  }

}
