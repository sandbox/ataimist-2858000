<?php

namespace Drupal\avalanche\Plugin\Avalanche\Swedish;

use Drupal\avalanche\StemmerBase;
use Drupal\avalanche\TokenizationInterface;
use Drupal\avalanche\StopwordInterface;

/**
 * Swedish Snowball stemmer.
 *
 * Stems Swedish words using the Snowball stemmer algorithm described at the
 * Snowball website. Additionally implements tokenization and stopwords, both
 * of which are experimental.
 *
 * @see http://snowballstem.org/algorithms/swedish/stemmer.html
 * @see Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishWord
 *
 * @Stemmer(
 *   id = "SwedishStemmer",
 *   languages = {
 *     "sv",
 *     "sv-SE",
 *     "sv-FI",
 *   },
 *   title = "Swedish"
 * )
 */
class SwedishStemmer extends StemmerBase implements TokenizationInterface, StopwordInterface {

  /**
   * List of stopwords that are optionally not stemmed.
   *
   * Stopwords that shouldn't be stemmed. The list originates from [1] and has
   * been converted into PHP array format to make using it easier and faster.
   *
   * @var array
   *
   * @see http://snowballstem.org/algorithms/swedish/stemmer.html [1]
   *
   * @todo Stopwords need to be configurable.
   */
  private static $stopwords = [
    'och', 'det', 'att', 'i', 'en', 'jag', 'hon', 'som', 'han', 'på', 'den',
    'med', 'var', 'sig', 'för', 'så', 'till', 'är', 'men', 'ett', 'om', 'hade',
    'de', 'av', 'icke', 'mig', 'du', 'henne', 'då', 'sin', 'nu', 'har', 'inte',
    'hans', 'honom', 'skulle', 'hennes', 'där', 'min', 'man', 'ej', 'vid',
    'kunde', 'något', 'från', 'ut', 'när', 'efter', 'upp', 'vi', 'dem', 'vara',
    'vad', 'över', 'än', 'dig', 'kan', 'sina', 'här', 'ha', 'mot', 'alla',
    'under', 'någon', 'eller', 'allt', 'mycket', 'sedan', 'ju', 'denna',
    'själv', 'detta', 'åt', 'utan', 'varit', 'hur', 'ingen', 'mitt', 'ni',
    'bli', 'blev', 'oss', 'din', 'dessa', 'några', 'deras', 'blir', 'mina',
    'samma', 'vilken', 'er', 'sådan', 'vår', 'blivit', 'dess', 'inom', 'mellan',
    'sådant', 'varför', 'varje', 'vilka', 'ditt', 'vem', 'vilket', 'sitta',
    'sådana', 'vart', 'dina', 'vars', 'vårt', 'våra', 'ert', 'era', 'vilkas',
  ];

  /**
   * Stems the given token.
   *
   * Goes through the stemming steps in the order defined by the Snowball
   * documentation.
   *
   * @param string $token
   *   The word to be stemmed.
   * @param string|null $langcode
   *   (optional) The language the word is in. Optional as StemmerInterface
   *   dictates, not used with this stemmer.
   *
   * @return \Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishWord
   *   The full token object; use ->getStem() to get the stemming result.
   */
  public function stem($token, $langcode = NULL) {
    $this->word = new SwedishWord($token);

    // Stemming is only done if the word has regions.
    if ($this->word->getRegions()) {
      $this->stepOne($this->word);
      $this->stepTwo($this->word);
      $this->stepThree($this->word);
    }

    return $this->word;
  }

  /**
   * Determines whether a given token is a stopword.
   *
   * Matches a word against the predefined stopword list. As a static function,
   * can be called without an existing SwedishStemmer instance.
   *
   * @param string $token
   *   The word to be checked.
   *
   * @return bool
   *   TRUE if the word is a stopword, FALSE if it isn't.
   *
   * @todo Could (should) be moved into a trait or base class.
   */
  public static function matchStopword($token) {
    static $tokens = [];

    // Normalize the token first, but don't do character replacements.
    $token = SwedishWord::normalize($token, FALSE);

    // If we've already checked this token, let's not do it again.
    if (isset($tokens[$token])) {
      return $tokens[$token];
    }

    $tokens[$token] = in_array($token, self::$stopwords);

    return $tokens[$token];
  }

  /**
   * Breaks a string down into one or more tokens.
   *
   * You can customize the produced tokens by supplying your own array of words
   * to discard ($discard_tokens). If you do so, note that prefixing a word in
   * the array with an asterisk causes only the specified case to be matched.
   * This is because the case of the first letter often has special meaning.
   *
   * You can also specify a minimum length for the produced tokens with the
   * $min_length parameter.
   *
   * @param string $string
   *   The string to be tokenized.
   * @param int $min_length
   *   (optional) Minimum length of returned token. Default: 3.
   * @param array $discard_tokens
   *   (optional) Array of tokens to be discarded.
   *
   * @return array
   *   An array of words extracted from the input string. Can be empty.
   *
   * @todo Does absolutely nothing at the moment.
   */
  public static function tokenize($string, $min_length = 3, array $discard_tokens = []) {
    return $string;
  }

  /**
   * Performs step 1 of the Snowball stemming.
   *
   * [quote]
   *  Search for the longest among the following suffixes in R1, and perform
   *  the action indicated.
   *
   *  a arna erna heterna orna ad e ade ande arne are aste en anden aren heten
   *  ern ar er heter or as arnas ernas ornas es ades andes ens arens hetens
   *  erns at andet het ast: delete.
   *
   *  s: delete if preceded by a valid s-ending (not necessarily in R1).
   * [/quote]
   *
   * @param \Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishWord $word
   *   The SwedishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/swedish/stemmer.html
   */
  private function stepOne(SwedishWord &$word) {
    $this->setStep(1);

    // Suffix length: 7
    // heterna: delete.
    if ($word->hasPattern('/(heterna)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-7);
    }
    // Suffix length: 6
    // hetens: delete.
    elseif ($word->hasPattern('/(hetens)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-6);
    }
    // Suffix length: 5
    // anden heten heter arnas ernas ornas andes arens andet: delete.
    elseif ($word->hasPattern('/(anden|heten|heter|arnas|ernas|ornas|andes|arens|andet)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-5);
    }
    // Suffix length: 4
    // arna erna orna ande arne aste aren ades erns: delete.
    elseif ($word->hasPattern('/(arna|erna|orna|ande|arne|aste|aren|ades|erns)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-4);
    }
    // Suffix length: 3
    // ade are ern ens het ast: delete.
    elseif ($word->hasPattern('/(ade|are|ern|ens|het|ast)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-3);
    }
    // Suffix length: 2
    // ad en ar er or as es at: delete.
    elseif ($word->hasPattern('/(ad|en|ar|er|or|as|es|at)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-2);
    }
    // Suffix length: 1
    // a e: delete.
    elseif ($word->hasPattern('/(a|e)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }
    // Suffix length: 1
    // s: delete if preceded by a valid s-ending.
    elseif ($word->hasPattern('/(?<=<%END%>)s$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }
  }

  /**
   * Performs step 2 of the Snowball stemming.
   *
   * [quote]
   *  Search for one of the following suffixes in R1, and if found delete the
   *  last letter: dd gd nn dt gt kt tt.
   * [/quote]
   *
   * @param \Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishWord $word
   *   The SwedishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/swedish/stemmer.html
   */
  private function stepTwo(SwedishWord &$word) {
    $this->setStep(2);

    if ($word->hasPattern('/(dd|gd|nn|dt|gt|kt|tt)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }
  }

  /**
   * Performs step 3 of the Snowball stemming.
   *
   * [quote]
   *  Search for the longest among the following suffixes in R1, and perform
   *  the action indicated.
   *
   *  lig ig els: delete.
   *  löst: replace with lös.
   *  fullt: replace with full.
   * [/quote]
   *
   * @param \Drupal\avalanche\Plugin\Avalanche\Swedish\SwedishWord $word
   *   The SwedishWord object to work on.
   *
   * @see http://snowballstem.org/algorithms/swedish/stemmer.html
   */
  private function stepThree(SwedishWord &$word) {
    $this->setStep(3);

    // Suffix length: 5
    // fullt: replace with full.
    if ($word->hasPattern('/(fullt)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }
    // Suffix length: 4
    // löst: replace with lös.
    elseif ($word->hasPattern('/(löst)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-1);
    }
    // Suffix length: 3
    // lig els: delete.
    elseif ($word->hasPattern('/(lig|els)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-3);
    }
    // Suffix length: 2
    // ig: delete.
    elseif ($word->hasPattern('/(ig)$/u', AVALANCHE_R1)) {
      $word->deleteSuffix(-2);
    }
  }

}
