<?php

namespace Drupal\avalanche\Plugin\Avalanche\Swedish;

use Drupal\avalanche\Token;
use Drupal\avalanche\NormalizationInterface;
use Drupal\Component\Utility\Unicode;

/**
 * Swedish token (word) class.
 *
 * Stores data about a single word, its regions and stem. Debug information on
 * the stemming process of the word is also stored, though not displayed by
 * default.
 */
class SwedishWord extends Token implements NormalizationInterface {

  /**
   * Swedish vowels in regex character set notation.
   *
   * Swedish vowels are a, e, i, o, u, y, ä, å and ö.
   *
   * @var string
   */
  private $vowel = '[aeiouyäåö]';
  /**
   * Characters that are not considered vowels for region determination.
   *
   * @var string
   */
  private $nonvowel = '[^aeiouyäåö]';
  /**
   * Valid s-endings per Snowball documentation.
   *
   * @var string
   */
  private $validEnding = '[bcdfghjklmnoprtvy]';

  /**
   * Initializes a SwedishWord instance.
   *
   * @param string $token
   *   The word to be stemmed.
   */
  public function __construct($token) {
    // Store the original word in $text and also set the given input as the
    // initial value of $stem - we don't yet know if it'll get stemmed at all,
    // as it might not have suitable regions to process.
    $this->word = $token;
    $this->stem = $token;

    // Set the named patterns found in regular expressions for Swedish.
    $this->namedPatterns = [
      // Vowels.
      '<%V%>' => $this->vowel,
      // Valid s-endings.
      '<%END%>' => $this->validEnding,
    ];

    // Class implements Normalization; accept a second parameter to determine
    // level of normalization and normalize the input word.
    if ($this instanceof NormalizationInterface) {
      // At minimum, the word is trimmed to eliminate useless whitespace and
      // some common punctuation.
      //
      // @todo
      // Per Porter Stemmer documentation, removing accents from letter-accent
      // combinations that do not form part of the alphabet of the language
      // would be useful.
      //
      // @see http://snowballstem.org/texts/introduction.html
      $this->stem = self::normalize($this->stem, FALSE);
    }

    // Determine the starting byte offset of region 1.
    $this->r1 = $this->getRegion();
    // Region 2 is not used in Swedish.
    $this->r2 = NULL;
  }

  /**
   * Normalizes a given token by e.g. converting it into lowercase.
   *
   * At minimum, converts the token into lowercase and removes any whitespace
   * around it. Does not touch punctuation (e.g. hyphens); those should have
   * been handled by the stemmer's tokenization process.
   *
   * @param string $token
   *   The token (word) to be normalized.
   *
   * @return string
   *   The normalized token.
   *
   * @todo Should normalize some accented characters; find out which and how.
   */
  public static function normalize($token) {
    // Trim excess whitespace and lowercase the token.
    $token = Unicode::strtolower(trim($token));

    return $token;
  }

  /**
   * Determines the regions as they are defined in the Snowball documentation.
   *
   * [quote from http://snowballstem.org/algorithms/swedish/stemmer.html]
   *  R1 is the region after the first non-vowel following a vowel, or is the
   *  null region at the end of the word if there is no such non-vowel. R1 is
   *  adjusted so that the region before it contains at least 3 letters. R2 is
   *  not used.
   * [/quote]
   *
   * @return int
   *   Region's starting byte (NOT char) offset; can also be 0 or last byte.
   */
  protected function getRegion() {
    // Array for holding matches (see preg_match documentation).
    $matches = [];
    // Pattern for finding R1. (?<=foo)bar matches bar when preceded by foo.
    // Notice the use of the /u switch - with Swedish, we deal with multibyte
    // characters much of the time.
    $pattern = '/(?<=' . $this->vowel . ')' . $this->nonvowel . '(.+)/u';

    // If there is no word to determine a region for, we can simply return NULL.
    // If a word has no regions, it won't get stemmed at all.
    if (empty($this->stem)) {
      return NULL;
    }

    // Find the next region, starting from byte offset $offset.
    if (preg_match($pattern, $this->stem, $matches, PREG_OFFSET_CAPTURE)) {
      // If a region is found, adjust it so that there are at least three
      // characters (not bytes) before it.
      $stem = Unicode::truncateBytes($this->stem, $matches[1][1]);
      // If there are enough characters, return the offset we found.
      if (Unicode::strlen($stem) >= 3) {
        return $matches[1][1];
      }
      // Otherwise, adjust it so that there are enough characters before it.
      else {
        // Return the byte offset of the 4th character.
        $offset = strlen(Unicode::substr($this->stem, 0, 3));
        return $offset;
      }
    }

    // If a region cannot be found, return the length of the string in bytes.
    // N.B. This will be larger than the character length in multibyte strings.
    // Using the byte length here is fully intentional.
    return strlen($this->stem);
  }

}
