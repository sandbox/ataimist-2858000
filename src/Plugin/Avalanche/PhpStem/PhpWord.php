<?php

namespace Drupal\avalanche\Plugin\Avalanche\PhpStem;

use Drupal\avalanche\Token;
use Drupal\Component\Utility\Unicode;

/**
 * Token (word) class for PECL stemmer.
 *
 * Stores data about a single word and its stem.
 */
class PhpWord extends Token {

  /**
   * The encoding of the source text. Always UTF-8 in Drupal.
   *
   * @var string
   */
  public $sourceEncoding = 'UTF-8';
  /**
   * The encoding expected by the stemmer for the token's language.
   *
   * @var string
   */
  public $targetEncoding;

  /**
   * Initializes a PhpWord instance.
   *
   * @param string $token
   *   The word to be stemmed.
   * @param string|null $langcode
   *   (optional) The language the word is in. Optional as StemmerInterface
   *   dictates, but no stemming will be done if language is not specified.
   */
  public function __construct($token, $langcode = NULL) {
    // Save initial token.
    $this->word = $token;
    // Trim excess whitespace and lowercase the token.
    $this->stem = Unicode::strtolower(trim($token));;

    // Unicode is acceptable to one of the Russian stemmers and the Turkish
    // stemmer. The other Russian stemmer uses KOI8-R and the Romanian stemmer
    // uses ISO-8859-2. All other stemmers use ISO-8859-1.
    switch ($langcode) {
      case 'ru':
        if (function_exists('stem_russian_unicode')) {
          $this->targetEncoding = NULL;
        }
        else {
          $this->targetEncoding = 'KOI8-R';
        }
        break;

      case 'ro':
        $this->targetEncoding = 'ISO-8859-2';
        break;

      case 'tr':
        $this->targetEncoding = NULL;
        break;

      default:
        $this->targetEncoding = 'ISO-8859-1';
        break;
    }

    $recode = !empty($this->targetEncoding);
    if (function_exists('mb_convert_encoding') && $recode) {
      $this->stem = mb_convert_encoding(
        $this->stem,
        $this->targetEncoding,
        $this->sourceEncoding
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getRegion($offset = 0) {
    // The PECL stemmers do not use regions.
    return strlen($this->stem);
  }

}
