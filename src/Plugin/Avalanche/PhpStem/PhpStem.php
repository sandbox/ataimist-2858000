<?php

namespace Drupal\avalanche\Plugin\Avalanche\PhpStem;

use Drupal\avalanche\StemmerBase;

/**
 * Avalanche stemmer that uses the PECL stem package if it is available.
 *
 * Uses the PECL stem package to stem various languages (exactly which ones is
 * determined in avalanche_avalanche_stemmers_alter in avalanche.module).
 * If the entire stem package has been compiled, it supports the following
 * languages and character sets:
 * - Danish (ISO-8859-1)
 * - Dutch (ISO-8859-1)
 * - English (ISO-8859-1, two stemmers: Porter and Porter2)
 * - Finnish (ISO-8859-1)
 * - French (ISO-8859-1)
 * - German (ISO-8859-1)
 * - Hungarian (ISO-8859-1? ISO-8859-2 would be better)
 * - Italian (ISO-8859-1)
 * - Norwegian (ISO-8859-1)
 * - Portuguese (ISO-8859-1)
 * - Romanian (ISO-8859-2)
 * - Russian (KOI8-R or Unicode, two stemmers)
 * - Spanish (ISO-8859-1)
 * - Swedish (ISO-8859-1)
 * - Turkish (Unicode)
 *
 * Note that, with the exception of the Russian and Turkish Unicode stemmers,
 * these stemmers do not expect input encoded as UTF-8 as supplied by Drupal
 * and will yield odd results if given such. The PhpWord class handles the
 * encoding conversion from UTF-8 to the target encoding, and the stem method
 * in this class converts the result back to UTF-8.
 *
 * @see https://pecl.php.net/package/stem
 *
 * @Stemmer(
 *   id = "PhpStem",
 *   languages = { },
 *   title = "PHP stemmer extension"
 * )
 */
class PhpStem extends StemmerBase {

  /**
   * Stems the given token.
   *
   * Goes through the stemming steps in the order defined by the Snowball
   * documentation.
   *
   * @param string $token
   *   The word to be stemmed.
   * @param string|null $langcode
   *   (optional) The language the word is in. Optional as StemmerInterface
   *   dictates, but no stemming will be done if language is not specified.
   *
   * @return \Drupal\avalanche\Plugin\Avalanche\PhpStem\PhpWord
   *   The full token object; use ->getStem() to get the stemming result.
   */
  public function stem($token, $langcode = NULL) {
    $this->word = new PhpWord($token, $langcode);

    $stemmers = [
      'en' => function_exists('stem_english') ? 'stem_english' : 'stem_porter',
      'fi' => 'stem_finnish',
      'sv' => 'stem_swedish',
      'da' => 'stem_danish',
      'nl' => 'stem_dutch',
      'fr' => 'stem_french',
      'de' => 'stem_german',
      'hu' => 'stem_hungarian',
      'no' => 'stem_norwegian',
      'nb' => 'stem_norwegian',
      'nn' => 'stem_norwegian',
      'pt' => 'stem_portuguese',
      'ro' => 'stem_romanian',
      'es' => 'stem_spanish',
      'ru' => function_exists('stem_russian_unicode') ? 'stem_russian_unicode' : 'stem_russian',
      'tr' => 'stem_turkish_unicode',
    ];

    if (!empty($langcode) && in_array($langcode, array_keys($stemmers))) {
      $this->word->setStem($stemmers[$langcode]($this->word->getStem()));
    }

    $recode = !empty($this->word->targetEncoding);
    if (function_exists('mb_convert_encoding') && $recode) {
      $this->word->setStem(
        mb_convert_encoding(
          $this->word->getStem(),
          $this->word->sourceEncoding,
          $this->word->targetEncoding
        )
      );
    }

    return $this->word;
  }

}
