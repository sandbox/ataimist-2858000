<?php

namespace Drupal\avalanche;

/**
 * Stemmer interface.
 */
interface StemmerInterface {

  /**
   * Initializes a stemmer.
   *
   * @param array $configuration
   *   Array of options to pass to the stemmer.
   * @param string $plugin_id
   *   ID of the stemmer (from annotation).
   * @param mixed $plugin_definition
   *   Plugin definition for the stemmer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition);

  /**
   * Stems a given word.
   *
   * Each stemmer should implement a stemming function that controls the
   * stemming process (runs the steps in the correct order &c.).
   *
   * @param string $token
   *   The word to stem.
   * @param string|null $langcode
   *   (optional) The language the word is in.
   */
  public function stem($token, $langcode = NULL);

}
