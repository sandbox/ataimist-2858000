<?php

namespace Drupal\avalanche;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Plugin\ConfigurablePluginInterface;
use Drupal\Component\Utility\NestedArray;

// Used as an offset parameter, denotes start of a token's first region.
define('AVALANCHE_R1', 'R1');
// Used as an offset parameter, denotes start of a token's second region.
define('AVALANCHE_R2', 'R2');

/**
 * Base stemmer class.
 */
abstract class StemmerBase extends PluginBase implements StemmerInterface, ConfigurablePluginInterface {

  /**
   * The Token object of the word being stemmed.
   *
   * @var \Drupal\avalanche\Token
   */
  protected $word;

  /**
   * Array of configuration information passed into the plugin.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * Array of step-stem pairs, e.g. ['one' => 'foobar', 'two' => 'foob'].
   *
   * This is mostly useful for debugging, and in some stemmers, seeing whether
   * a change was made between two steps which should, in turn, affect a third.
   *
   * @var array
   */
  protected $step = [];

  /**
   * Stores the current word stem in a given step identifier.
   *
   * @param string $step
   *   The step identifier to store the stem in. Always used as a string.
   */
  protected function setStep($step) {
    $this->step[(string) $step] = $this->word->getStem();
  }

  /**
   * Gets the word stem as it was at the given predefined point.
   *
   * @param string $step
   *   The step identifier to get the stem for. Always used as a string.
   *
   * @return string|bool
   *   The stem as it was at the given step, or FALSE if not found.
   */
  protected function getStep($step) {
    $output = FALSE;

    if (isset($this->step[(string) $step])) {
      $output = $this->step[(string) $step];
    }

    return $output;
  }

  /**
   * Initializes a stemmer plugin instance.
   *
   * Default inherited constructor for Avalanche stemmer plugins.
   * Can be overridden in plugin classes if needed.
   *
   * @param array $configuration
   *   Array of extra options for this instance, such as a non-standard list of
   *   stopwords or discardable tokens. Recognized array keys are as follows:
   *   - stopword
   *     Alternative array of stopwords that won't get stemmed.
   *   - tokenization
   *     Alternative array of tokens that will be discarded in tokenization.
   * @param string $plugin_id
   *   ID of the plugin being initialized (from plugin definition).
   * @param mixed $plugin_definition
   *   Definition of the plugin being initialized.
   *
   * @todo
   *   Account for configuration. Would be nice to do this in each separate
   *   add-on interface (e.g. \Drupal\avalanche\StopwordInterface would handle
   *   changing the stopword list.
   */
  public function __construct(array $configuration = [], $plugin_id = NULL, $plugin_definition = NULL) {
    $this->setConfiguration($configuration);
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $plugin_definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $this->configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'disable' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

}
